from openerp.osv import fields, osv


class SaleOrder(osv.osv):
    _inherit = "sale.order"
    _columns = {
        'name': fields.char('Name', size=64, required=True, translate=True)
    }
